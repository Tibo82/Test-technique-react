import React from 'react';
import Header from './modules/components/Header';
import Layout from './modules/components/Layout';
import Routing from './modules/routes/Routing';
import { AppProvider } from './modules/components/Todos/helpers/context';

const App = () => (
  <div>
    <AppProvider>
      <Header />
      <Layout>
        <Routing />
      </Layout>
    </AppProvider>
  </div>
);
export default App;
