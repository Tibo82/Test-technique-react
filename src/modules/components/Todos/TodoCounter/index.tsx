import React, { useContext } from 'react';
import { AppContext } from '../helpers/context';
import { Types } from '../helpers/reducers';

const TodoCounter = () => {
  const { state, dispatch } = useContext(AppContext);
  const closeAllTodos = () => {
    console.log('state', state);
    dispatch({
      type: Types.CloseAllTodo,
      payload: {
        id: 'id',
      },
    });
  };
  return (
    <div className="CounterStyle">
      <h3>Todo still open :
        <input
          value={state.counter}
          onClick={closeAllTodos}

          type="text"
          readOnly
        />
      </h3>
    </div>
  );
};
export default TodoCounter;
