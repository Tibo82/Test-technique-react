import React, { useContext } from 'react';
import { AppContext } from '../helpers/context';
import { Types } from '../helpers/reducers';

type TodoContentProps = {
  id: string;
  content: string;
  isCompleted: boolean;
};

const TodoContent = ({ id, content, isCompleted }: TodoContentProps) => {
  const { dispatch } = useContext(AppContext);

  const CloseItem = (ItemId: string) => {
    dispatch({
      type: Types.CloseTodo,
      payload: {
        id: ItemId,
      },
    });
    dispatch({
      type: Types.UpdateCountTodoOpen,
      payload: {
        increment: -1,
      },
    });
  };
  const OpenItem = (ItemId: string) => {
    dispatch({
      type: Types.OpenTodo,
      payload: {
        id: ItemId,
      },
    });
    dispatch({
      type: Types.UpdateCountTodoOpen,
      payload: {
        increment: 1,
      },
    });
  };
  return (
    <div>
      <div className="ContentArea">
        <textarea id={id} value={content} readOnly />
        {isCompleted === false ? (
          <input id={id} type="submit" value="close item" onClick={() => CloseItem(id)} readOnly />
        ) : (
          <input id={id} type="submit" value="re-open item" onClick={() => OpenItem(id)} readOnly />
        )}
      </div>
    </div>
  );
};

export default TodoContent;
