import React, { useContext } from 'react';
import TodoContent from './TodoContent';
import { AppContext } from '../helpers/context';
import { Types } from '../helpers/reducers';
import Tick from '../../../assets/Tick';

type TodoItemProps = {
  id: string;
  title: string;
  content: string;
  isCompleted: boolean;
  showContent:boolean;
};

const TodoItem = ({
  title, content, id, isCompleted, showContent,
}: TodoItemProps) => {
  const { state, dispatch } = useContext(AppContext);

  const show = (itemId: string) => {
    const todo = state.items.filter((item) => item.id === id);
    if (!todo[0].showContent) {
      dispatch({
        type: Types.ShowTodoContent,
        payload: {
          id: itemId,
        },
      });
    } else {
      dispatch({
        type: Types.HideTodoContent,
        payload: {
          id,
        },
      });
    }
  };
  const removeTodo = (itemId: string) => {
    dispatch({
      type: Types.DeleteTodo,
      payload: {
        id: itemId,
      },
    });
    if (!isCompleted) {
      dispatch({
        type: Types.UpdateCountTodoOpen,
        payload: {
          increment: -1,
        },
      });
    }
  };
  return (
    <div>
      <div className="ItemAndCloser">
        <input
          id={id}
          value={title}
          onClick={() => show(id)}
          className="TitleInput"
          type="text"
          readOnly
        />

        {isCompleted && <div className="TickItemclosed"><Tick /></div> }

        <button id={id} onClick={() => removeTodo(id)} className="DeleteButton" type="submit">
          x
        </button>

      </div>
      {showContent && <TodoContent id={id} content={content} isCompleted={isCompleted} />}
    </div>
  );
};

export default TodoItem;
