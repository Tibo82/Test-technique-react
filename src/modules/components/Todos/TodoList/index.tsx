/* eslint-disable @typescript-eslint/quotes */
import React, { useState, useContext, useEffect } from "react";

import { AppContext } from "../helpers/context";
import { Types } from "../helpers/reducers";
import fetchLocalStore from "../helpers/fetchLocalStore";

import TodoItem from "./TodoItem";

const TodoList = () => {
  const [textContent, setTextContent] = useState("");
  const [textTitle, setTextTitle] = useState("");
  const { state, dispatch } = useContext(AppContext);

  useEffect(() => {
    const getLocalStore = async function fetchData() {
      const localStore = await fetchLocalStore();
      dispatch({
        type: Types.ImportLocalStore,
        payload: { items: localStore.items },
      });
      const countTodoOpen = localStore.items.filter((item: any) => item.isCompleted === false);
      dispatch({
        type: Types.UpdateCountTodoOpen,
        payload: {
          increment: countTodoOpen.length,
        },
      });
    };
    getLocalStore();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const newTodo = () => {
    dispatch({
      type: Types.AddTodo,
      payload: {
        title: textTitle,
        content: textContent,
      },
    });
    dispatch({
      type: Types.UpdateCountTodoOpen,
      payload: {
        increment: 1,
      },
    });
    setTextTitle("");
    setTextContent("");
  };
  return (
    <div className="TodoList">
      <div className="TodoAddElements">
        <input
          type="text"
          value={textTitle}
          placeholder="Title ..."
          onChange={(e) => setTextTitle(e.target.value)}
        />
        <textarea
          placeholder="Content (description)..."
          value={textContent}
          onChange={(e) => setTextContent(e.target.value)}
        />
        <button type="submit" onClick={newTodo}>
          ADD
        </button>
      </div>
      <div className="ListItems">
        <ul>
          {state.items.map((item) => (
            <li key={item.id}>
              <TodoItem
                title={item.title}
                content={item.content}
                id={item.id}
                isCompleted={item.isCompleted}
                showContent={item.showContent}
              />
            </li>
          ))}
        </ul>
      </div>

    </div>
  );
};

export default TodoList;
