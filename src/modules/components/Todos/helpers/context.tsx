import React, { createContext, useReducer, Dispatch } from 'react';

import {
  itemReducer, ItemActions, counterReducer, CounterActions
} from './reducers';

type ItemType = {
  id: string;
  title: string;
  content: string;
  isCompleted: boolean;
  showContent:boolean;
};

type InitialStateType = {
  items:ItemType[];
  counter:number;
};

export const initialState = {
  items: [],
  counter: 0,
};

const AppContext = createContext<{
  state:InitialStateType;
  dispatch:Dispatch<ItemActions | CounterActions>
}>({
  state: initialState,
  dispatch: () => null
});

const mainReducer = (
  { items, counter }: InitialStateType,
  action: ItemActions | CounterActions
) => ({
  items: itemReducer(items, action as ItemActions),
  counter: counterReducer(counter, action as CounterActions),
});

const AppProvider:React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(mainReducer, initialState);
  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {children}
    </AppContext.Provider>

  );
};

export { AppContext, AppProvider };
