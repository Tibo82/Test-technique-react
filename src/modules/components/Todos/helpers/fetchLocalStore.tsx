/* eslint-disable @typescript-eslint/quotes */
import { initialState } from "./context";

const fetchLocalStore = async () => {
  try {
    const response = await fetch('localStore.json');
    const LocalStore = await response.json();
    return LocalStore;
  } catch (error) {
    return initialState;
  }
};

export default fetchLocalStore;
