/* eslint-disable @typescript-eslint/quotes */
import uniqid from 'uniqid';

type ActionMap<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined ? { type: Key } : { type: Key; payload: M[Key] };
};

export enum Types {
  AddTodo = "ADD_TODO",
  DeleteTodo = "DELETE_TODO",
  CloseTodo = "CLOSE_TODO",
  OpenTodo = "OPEN_TODO",
  ShowTodoContent = "SHOW_TODO_CONTENT",
  HideTodoContent = "HIDE_TODO_CONTENT",
  UpdateCountTodoOpen = "UPDATE_COUNT_TODO_OPEN",
  DecrementTodoOpen = "DECREMENT_TODO_OPEN",
  CloseAllTodo = "CLOSE_ALL_TODO",
  ImportLocalStore = "IMPORT_INITIAL_STORE",
}

// item

type ItemType = {
  id: string;
  title: string;
  content: string;
  isCompleted: boolean;
  showContent: boolean;
};

type ItemPayload = {
  [Types.AddTodo]: {
    title: string;
    content: string;
  };
  [Types.DeleteTodo]: {
    id: string;
  };
  [Types.CloseTodo]: {
    id: string;
  };
  [Types.OpenTodo]: {
    id: string;
  };
  [Types.ShowTodoContent]: {
    id: string;
  };
  [Types.HideTodoContent]: {
    id: string;
  };
  [Types.CloseAllTodo]: {
    id: string;
  };
  [Types.ImportLocalStore]: {
    items: ItemType[];
  };
};

export type ItemActions = ActionMap<ItemPayload>[keyof ActionMap<ItemPayload>];

export const itemReducer = (state: ItemType[], action: ItemActions) => {
  switch (action.type) {
    case Types.AddTodo:
      return [
        ...state,
        {
          id: uniqid(),
          title: action.payload.title,
          content: action.payload.content,
          isCompleted: false,
          showContent: false,
        },
      ];
    case Types.DeleteTodo: {
      return [...state.filter((item) => item.id !== action.payload.id)];
    }

    case Types.CloseTodo: {
      state.map((item) => {
        if (item.id === action.payload.id) {
          item.isCompleted = true;
          item.showContent = false;
        }
        return item;
      });

      return state;
    }
    case Types.OpenTodo: {
      state.map((item) => {
        if (item.id === action.payload.id) {
          item.isCompleted = false;
          item.showContent = false;
        }
        return item;
      });

      return state;
    }
    case Types.ShowTodoContent: {
      state.map((item) => {
        if (item.id === action.payload.id) {
          item.showContent = true;
        }
        return item;
      });

      return state;
    }
    case Types.HideTodoContent: {
      state.map((item) => {
        if (item.id === action.payload.id) {
          item.showContent = false;
        }
        return item;
      });

      return state;
    }
    case Types.CloseAllTodo: {
      state.map((item) => (item.isCompleted = true));

      return state;
    }
    case Types.ImportLocalStore: {
      return (state = action.payload.items);
    }

    default:
      return state;
  }
};

// counter

type CounterPayload = {
  [Types.UpdateCountTodoOpen]: {
    increment: number;
  };
  [Types.CloseAllTodo]: undefined;
};
export type CounterActions = ActionMap<CounterPayload>[keyof ActionMap<CounterPayload>];

export const counterReducer = (state: number, action: CounterActions) => {
  switch (action.type) {
    case Types.UpdateCountTodoOpen: {
      return state + action.payload.increment;
    }
    case Types.CloseAllTodo: {
      return (state = 0);
    }

    default:
      return state;
  }
};
