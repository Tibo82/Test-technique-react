import './index.scss';
import React from 'react';
import TodoList from './TodoList';
import TodoCounter from './TodoCounter';

const Todos = () => (
  <div className="Todo">
    <h1>Todos App</h1>
    <TodoCounter />
    <TodoList />
  </div>
);
export default Todos;
