import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { getRoutes } from './getRoutes';

const UrlLinks = () => {
  const linkComponents = getRoutes.map(({ path, name, id }) => (name ? (
    <li key={id}>
      <a href={path}>{name}</a>
    </li>
  ) : (
    null
  )));

  return (
    <Router>
      <div>
        <ul>{linkComponents}</ul>
      </div>
    </Router>
  );
};

export default UrlLinks;
