/* eslint-disable @typescript-eslint/quotes */
import uniqid from 'uniqid';
import Home from '../pages/Home';
import Todos from '../components/Todos';
import NotFoundPage from '../pages/NotFoundPage';

export const getRoutes = [
  {
    path: "/",
    component: Home,
    id: uniqid(),
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
    id: uniqid(),
  },
  {
    path: "/todos",
    name: "Todos",
    component: Todos,
    id: uniqid(),
  },
  {
    path: "*",
    component: NotFoundPage,
    id: uniqid(),
  },
];
